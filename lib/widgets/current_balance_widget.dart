import 'package:flutter/material.dart';
import 'package:visa_appp/screens/10-currencymanage.dart';

// ignore: must_be_immutable
class CurrentBalanceWidget extends StatefulWidget {
  String flagImage;
  String currencySymbol;
  String currencyBalance;
  String currencyName;
  CurrentBalanceWidget(this.flagImage, this.currencySymbol,
      this.currencyBalance, this.currencyName);
  @override
  _CurrentBalanceWidgetState createState() => _CurrentBalanceWidgetState();
}

class _CurrentBalanceWidgetState extends State<CurrentBalanceWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
            leading: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Container(
                  height: 31, width: 30, child: Image.asset(widget.flagImage)),
            ),
            title: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: widget.currencySymbol,
                    style: TextStyle(
                        fontFamily: 'Poppins-SemiBold',
                        fontSize: 12.0,
                        color: Color(0xff9d9da9)),
                  ),
                  TextSpan(
                    text: widget.currencyBalance,
                    style: TextStyle(
                        fontFamily: 'Poppins-Light',
                        fontSize: 10.0,
                        color: Color(0xff1c1d22)),
                  ),
                ],
              ),
            ),
            subtitle: Text(
              widget.currencyName,
              style: TextStyle(
                  fontFamily: 'Poppins-Medium',
                  fontSize: 12.0,
                  color: Color(0xff1c1d22)),
            ),
            trailing: Wrap(
              spacing: 12,
              children: [
                Text(
                  'manage',
                  style: TextStyle(
                      fontFamily: 'Poppins-Regular',
                      fontSize: 10.0,
                      color: Color(0xff616267)),
                ),
                Container(
                    height: 11,
                    width: 6,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CurrencyManage()));
                      },
                      child: Image.asset("images/arrow.png"),
                    )),
              ],
            )),
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Divider(
            thickness: 1,
            color: Color(0xffe3e3e5),
          ),
        )
      ],
    );
  }
}
