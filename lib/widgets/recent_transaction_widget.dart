import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RecentTransactionWidget extends StatefulWidget {
  String iconImage;
  String transName;
  String transdate;
  String transBalance;
  RecentTransactionWidget(
      this.iconImage, this.transBalance, this.transName, this.transdate);

  @override
  _RecentTransactionWidgetState createState() =>
      _RecentTransactionWidgetState();
}

class _RecentTransactionWidgetState extends State<RecentTransactionWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Color(0xffe3e3e5),
                ),
                borderRadius: BorderRadius.circular(15)),
            child: ListTile(
                leading: Container(
                    height: 40,
                    width: 40,
                    child: Image.asset(widget.iconImage)),
                title: Text(
                  widget.transName,
                  style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Color(0xff1c1d22),
                  ),
                ),
                subtitle: Text(
                  widget.transdate,
                  style: TextStyle(
                    fontFamily: "Poppins-Medium",
                    fontSize: 10,
                    color: Color(0xff9d9da9),
                  ),
                ),
                trailing: Wrap(
                  spacing: 12,
                  children: [
                    Text(
                      widget.transBalance,
                      style: TextStyle(
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                        color: Color(0xff1c1d22),
                      ),
                    ),
                    Container(
                        height: 11,
                        width: 6,
                        child: Image.asset("images/arrow1.png")),
                  ],
                ))));
  }
}
