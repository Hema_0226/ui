import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ExpenseWidget extends StatefulWidget {
  String currencyName;
  ExpenseWidget(this.currencyName);

  @override
  _ExpenseWidgetState createState() => _ExpenseWidgetState();
}

class _ExpenseWidgetState extends State<ExpenseWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
        child: Container(
            height: 49,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xffe3e3e5)),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Row(children: [
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Container(
                  height: 49,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Color(0xffe3e3e5)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        widget.currencyName,
                        style: TextStyle(
                          fontFamily: "Poppins-Medium",
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 226, right: 15),
                      child: Container(
                          height: 14,
                          width: 8,
                          child: Image.asset("images/arrow.png")),
                    ),
                  ]),
                ),
              )
            ])));
  }
}
