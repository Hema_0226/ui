import 'package:flutter/material.dart';
import 'package:visa_appp/screens/11-exchangepage.dart';

class AtmLocator extends StatefulWidget {
  @override
  _AtmLocatorState createState() => _AtmLocatorState();
}

class _AtmLocatorState extends State<AtmLocator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Stack(children: [
      Container(
        child: Image(
          image: AssetImage("images/rectangle_copy.png"),
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 64, left: 20, right: 20),
        child: Row(
          children: [
            Container(
                height: 16, width: 9, child: Image.asset("images/back.png")),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 122),
                child: Text(
                  'ATM Locator',
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontFamily: "Poppins-Regular",
                    fontSize: 12,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 12, bottom: 21),
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 108, left: 20),
                child: Text(
                  'showing 20 ATM’s nearby',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Poppins-Regular",
                    fontSize: 12,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 106),
              child: Text(
                'sort & filter',
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontFamily: "poppins-Bold",
                  fontSize: 14,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 106, right: 20, left: 7),
              child: Container(
                  height: 10,
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ExchangePage()));
                      },
                      child: Image.asset("images/sort.png"))),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 146),
        child: Container(child: Image.asset('images/map.png')),
      ),
    ])));
  }
}
