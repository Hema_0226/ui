import 'package:flutter/material.dart';
import 'package:visa_appp/screens/20-preferences.dart';

class ConfirmationScreen extends StatefulWidget {
  ConfirmationScreen({Key key}) : super(key: key);

  @override
  _ConfirmationScreenState createState() => _ConfirmationScreenState();
}

class _ConfirmationScreenState extends State<ConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff5f5f7),
        body: SingleChildScrollView(
            child: Stack(children: [
          Container(
            child: Image(
              image: AssetImage("images/bg.png"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 64, left: 20, right: 20),
            child: Row(
              children: [
                Container(
                    height: 16,
                    width: 9,
                    child: Image.asset("images/back.png")),
                Expanded(
                  child: Center(
                    child: Text(
                      'Confirmation',
                      style: TextStyle(
                        color: Color(0xffffffff),
                        fontFamily: "Poppins-Regular",
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(
                  top: 106.0, left: 20.0, right: 20.0, bottom: 20.0),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 1.0,
                      color: Color(0xffe3e3e5),
                    ),
                    borderRadius: BorderRadius.circular(15.0)),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 15,
                  ),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 10),
                      child: Row(children: [
                        Text(
                          'confirm Exchange',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 152, right: 20),
                          child: Text(
                            'EDIT',
                            style: TextStyle(
                              color: Color(0xff1a1f71),
                              fontFamily: "Poppins-Medium",
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14.5),
                      child: Divider(
                        thickness: 1,
                        color: Color(0xffb9b9bf),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 19.5),
                      child: Text(
                        '\$400',
                        style: TextStyle(
                          fontFamily: "Poppins-Light",
                          fontSize: 30,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        'Balance: \$1,319',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 10,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Container(
                          height: 8,
                          width: 14,
                          child: Image.asset("images/down.png")),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        '€361.44',
                        style: TextStyle(
                          fontFamily: "Poppins-Light",
                          fontSize: 30,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        'Balance: €20',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 13,
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      color: Color(0xffb9b9bf),
                    ),
                    ListTile(
                      title: Text(
                        'From',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 13,
                        ),
                      ),
                      trailing: Text(
                        '\$USD',
                        style: TextStyle(
                          color: Color(0xff1c1d22),
                          fontFamily: "Poppins-Medium",
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'To',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 13,
                        ),
                      ),
                      trailing: Text(
                        '€EUR',
                        style: TextStyle(
                          color: Color(0xff1c1d22),
                          fontFamily: "Poppins-Medium",
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Exchange Rate',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 13,
                        ),
                      ),
                      trailing: Text(
                        '1 USD = 0.9 EUR',
                        style: TextStyle(
                          color: Color(0xff1c1d22),
                          fontFamily: "Poppins-Medium",
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Fee',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Medium",
                          fontSize: 13,
                        ),
                      ),
                      trailing: Text(
                        'Applied',
                        style: TextStyle(
                          color: Color(0xff1c1d22),
                          fontFamily: "Poppins-Medium",
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 272),
                      child: Text(
                        'click to see',
                        style: TextStyle(
                          color: Color(0xff9d9da9),
                          fontFamily: "Poppins-Regular",
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ]),
                ),
              )),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 641, left: 20, right: 20),
                child: Container(
                  height: 42,
                  width: 355,
                  decoration: BoxDecoration(
                      color: Color(0xff0066cc),
                      borderRadius: BorderRadius.circular(15)),
                  child: Center(
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CardPreferences()));
                      },
                      child: Text(
                        'Exchange Now',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontFamily: "Poppins-SemiBold",
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  'cancel',
                  style: TextStyle(
                    color: Color(0xff616267),
                    fontFamily: "Poppins-SemiBold",
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ])));
  }
}
