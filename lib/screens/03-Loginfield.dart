import 'package:flutter/material.dart';
import 'package:visa_appp/screens/04-Loginpin.dart';

class LoginField extends StatefulWidget {
  @override
  _LoginFieldState createState() => _LoginFieldState();
}

class _LoginFieldState extends State<LoginField> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
            key: _formkey,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 224.0, left: 112.0, right: 112.0),
                    child: Container(
                      height: 70,
                      width: 151,
                      child: Image(
                        image: AssetImage('images/logo_splash.png'),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30.0, left: 71.0, right: 71.0),
                    child: Center(
                      child: Text(
                        'Get Started By Logging in!',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: "Poppins-Medium",
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 118.0, left: 30.0),
                            child: Text(
                              'CUSTOMER ID',
                              style: TextStyle(
                                color: Color(0xff9d9da9),
                                fontSize: 10.0,
                                fontFamily: 'Poppins-Medium',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, right: 20.0, left: 20),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Color(0xffb9b9bf),
                                ),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextFormField(
                                autofillHints: [AutofillHints.email],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter your mail id/Mobile no'),
                                // // ignore: missing_return
                                // validator: (String value) {
                                //   if (value.isEmpty) {
                                //     return 'This field is required';
                                //   }
                                //   if (!value.contains('@')) {
                                //     return "A valid email should contain '@'";
                                //   }
                                //   if (!RegExp(
                                //     r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
                                //   ).hasMatch(value)) {
                                //     return "Please enter a valid email";
                                //   }
                                // },
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 20.0, left: 30.0),
                            child: Text(
                              'PASSWORD',
                              style: TextStyle(
                                color: Color(0xff9d9da9),
                                fontSize: 10.0,
                                fontFamily: 'Poppins-Medium',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, right: 20.0, left: 20.0),
                            child: Container(
                              height: 55.0,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Color(0xffb9b9bf),
                                ),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextFormField(
                                obscureText: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter the password'),
                                // validator: (String value) {
                                //   Pattern pattern =
                                //       r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
                                //   RegExp regex = new RegExp(pattern);
                                //   print(value);
                                //   if (value.isEmpty) {
                                //     return 'Please enter password';
                                //   } else {
                                //     if (!regex.hasMatch(value))
                                //       return 'Enter valid password';
                                //     else
                                //       return null;
                                //   }
                                // }
                              ),
                            ),
                          ),
                        ]),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 245),
                    child: Text(
                      'Set/ForgotPassword?',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: 'Poppins-Medium',
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30.0, bottom: 32.0, right: 20.0, left: 20.0),
                    child: Container(
                      height: 42.0,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Color(0xff0066cc),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Center(
                        child: InkWell(
                          onTap: () {
                            // if (!_formkey.currentState.validate()) {
                            //   return;
                            // }
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPin()));
                          },
                          child: Text(
                            'Login',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins-SemiBold',
                              fontSize: 12.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 52.0, right: 51.0, bottom: 40.0),
                    child: Center(
                      child: RichText(
                        text: (TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  'By clicking login, you agree to our Terms & Conditions',
                              style: TextStyle(
                                  fontFamily: 'Poppins-Regular',
                                  fontSize: 10.0,
                                  color: Colors.grey),
                            ),
                          ],
                        )),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
