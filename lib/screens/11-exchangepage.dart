import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:visa_appp/screens/12-confirmationscreen.dart';

class ExchangePage extends StatefulWidget {
  @override
  _ExchangePageState createState() => _ExchangePageState();
}

class _ExchangePageState extends State<ExchangePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Stack(children: [
      Container(
        child: Image(
          image: AssetImage("images/bg.png"),
          fit: BoxFit.fill,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 59, left: 20),
        child: Row(
          children: [
            Container(
                height: 16, width: 9, child: Image.asset("images/back.png")),
            Padding(
              padding: const EdgeInsets.only(left: 130),
              child: Text(
                'Exchange USD',
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontFamily: "Popins-Regular",
                  fontSize: 12,
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(
            top: 106.0, left: 20.0, right: 20.0, bottom: 20.0),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                width: 1.0,
                color: Color(0xfff5f5f7),
              ),
              borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            children: [
              Row(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                  child: Text(
                    'Exchange Money',
                    style: TextStyle(
                      color: Color(0xff1c1d22),
                      fontFamily: "Poppins-SemiBold",
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ]),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 20.0, right: 120.0, bottom: 19.5),
                    child: Container(
                      width: 200,
                      child: Text(
                        'Find Out real-timeExchange rates & Exchange between yours balances',
                        style: TextStyle(
                            fontFamily: "Poppins-Medium",
                            fontSize: 10.0,
                            letterSpacing: 0),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                thickness: 1,
                color: Color(0xffb9b9bf),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 14.5),
                child: ListTile(
                  title: Wrap(
                    spacing: 12,
                    children: [
                      Text(
                        'USD',
                        style: TextStyle(
                          fontFamily: "Poppins-Light",
                          fontSize: 30,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12.5),
                        child: Container(
                            height: 8,
                            width: 14,
                            child: Image.asset("images/down.png")),
                      ),
                    ],
                  ),
                  subtitle: Text(
                    'Balance \$139',
                    style: TextStyle(
                        fontFamily: "Poppins-Medium",
                        fontSize: 10.0,
                        letterSpacing: 0),
                  ),
                  trailing: Text(
                    '1',
                    style: TextStyle(
                        fontFamily: "Poppins-Light",
                        fontSize: 30.0,
                        letterSpacing: 0),
                  ),
                ),
              ),
              Container(
                child: Stack(alignment: Alignment.center, children: [
                  Divider(
                    color: Color(0xffb9b9bf),
                    thickness: 1,
                    indent: 1.0,
                  ),
                  ListTile(
                    title: Chip(
                      label: Text(
                        '1=0.91',
                        style: TextStyle(
                            fontFamily: "Poppins-SemiBold", fontSize: 10.0),
                      ),
                    ),
                  ),
                ]),
              ),
              ListTile(
                title: Wrap(
                  spacing: 12,
                  children: [
                    Text(
                      'EUR',
                      style: TextStyle(
                        fontFamily: "Poppins-Light",
                        fontSize: 30,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 12.5),
                      child: Container(
                          height: 8,
                          width: 14,
                          child: Image.asset("images/down.png")),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Balance: €20',
                  style: TextStyle(
                      fontFamily: "Poppins-Medium",
                      fontSize: 10.0,
                      letterSpacing: 0),
                ),
                trailing: Text(
                  '0.61',
                  style: TextStyle(
                      fontFamily: "Poppins-Light",
                      fontSize: 30.0,
                      letterSpacing: 0),
                ),
              ),
              Divider(
                thickness: 1,
                color: Color(0xffb9b9bf),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 14, left: 20, right: 215, bottom: 20),
                child: Container(
                  height: 36,
                  width: 130,
                  decoration: BoxDecoration(
                      color: Color(0xff0066cc),
                      borderRadius: BorderRadius.circular(15)),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ConfirmationScreen()));
                      },
                      child: Text(
                        'exchange now',
                        style: TextStyle(
                            color: Color(0xffffffff),
                            fontFamily: "Poppins-SemiBold",
                            fontSize: 12.0,
                            letterSpacing: 0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ])));
  }
}
