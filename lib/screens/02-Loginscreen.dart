import 'package:flutter/material.dart';
import 'package:visa_appp/screens/03-Loginfield.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 224.0),
              child: Container(
                height: 70.0,
                width: 151,
                child: Image(
                  image: AssetImage('images/logo_splash.png'),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 1),
              child: Text(
                'Get started by Logging in!',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: "Poppins-Medium",
                  color: Color(0xff1c1d22),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 91.0),
              child: Text(
                'FINGERPRINT UNLOCK',
                style: TextStyle(
                  color: Color(0xff9d9da9),
                  fontFamily: 'Poppins-Medium',
                  fontSize: 10,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 1, top: 10),
              child: Container(
                width: 200.0,
                height: 200.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 5,
                      color: Colors.grey[200],
                    ),
                    borderRadius: BorderRadius.circular(10)),
                child: Image(
                  image: AssetImage('images/fingerprint.png'),
                  width: 80.0,
                  height: 80.0,
                ),
              ),
            ),
            SizedBox(
              height: 14,
            ),
            Text(
              'or',
              style: TextStyle(
                fontFamily: "Poppins-Regular",
                fontSize: 13,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 60.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginField()));
                },
                child: Text(
                  'Login with Customer ID',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13.0,
                    fontFamily: 'Poppins-Medium',
                    color: Color(0xff1c1d22),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
