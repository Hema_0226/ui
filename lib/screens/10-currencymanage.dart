import 'package:flutter/material.dart';
import 'package:visa_appp/screens/06-Expense.dart';
import 'package:visa_appp/widgets/recent_transaction_widget.dart';

class CurrencyManage extends StatefulWidget {
  CurrencyManage({Key key}) : super(key: key);

  @override
  _CurrencyManageState createState() => _CurrencyManageState();
}

class _CurrencyManageState extends State<CurrencyManage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff5f5f7),
        body: Stack(children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: double.infinity,
            child: Image(
              fit: BoxFit.fill,
              image: AssetImage("images/bg.png"),
            ),
          ),
          SingleChildScrollView(
              child: Column(children: [
            Padding(
              padding: const EdgeInsets.only(top: 59, left: 20, right: 20),
              child: Row(
                children: [
                  Container(height: 16, child: Image.asset("images/back.png")),
                  Expanded(
                    child: Center(
                      child: Text(
                        'USD Balance',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontFamily: "Poppins-Regular",
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 30.0, left: 20.0, right: 20.0, bottom: 20.0),
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          width: 1.0,
                          color: Color(0xffe3e3e5),
                        ),
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(children: [
                      Container(
                        height: 45,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Color(0xfff5f5f7),
                            borderRadius: BorderRadius.circular(15)),
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 15, top: 15, bottom: 15),
                                child: Text(
                                  'YES BANK-2134',
                                  style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: 10,
                                    color: Color(0xff1c1d22),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Container(
                                height: 10,
                                width: 31,
                                child: Image(
                                  image: AssetImage("images/visa.png"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30),
                        child: Image.asset("images/usa.png"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Text(
                          'CURRENT BALANCE',
                          style: TextStyle(
                            color: Color(0xff9d9da9),
                            fontFamily: "Poppins-Medium",
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Text(
                          '\$599.91',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Poppins-Medium",
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30),
                        child: Divider(
                          thickness: 1,
                          color: Color(0xffb9b9bf).withOpacity(.4),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 20.5, top: 19.5),
                        child: Row(
                          children: [
                            Container(
                              height: 36,
                              width: 147,
                              decoration: BoxDecoration(
                                color: Color(0xff0066cc),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Center(
                                child: Text(
                                  'exchange money',
                                  style: TextStyle(
                                      color: Color(0xffffffff),
                                      fontFamily: "Poppins-SemiBold",
                                      fontSize: 12),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 27),
                              child: Container(
                                height: 36,
                                width: 121,
                                decoration: BoxDecoration(
                                  color: Color(0xff616267),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Center(
                                  child: Text(
                                    'card settings',
                                    style: TextStyle(
                                        color: Color(0xffffffff),
                                        fontFamily: "Poppins-SemiBold",
                                        fontSize: 12),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ]))),
            Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'recent transactions',
                        style: TextStyle(
                            fontFamily: 'Poppins-Regular',
                            fontSize: 12.0,
                            color: Color(0xff616267)),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ExpenseScreen()));
                      },
                      child: Row(
                        children: [
                          Text(
                            'viewall',
                            style: TextStyle(
                                fontFamily: 'Poppins-Medium',
                                fontWeight: FontWeight.bold,
                                fontSize: 10.0,
                                color: Color(0xff616267)),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Container(
                          height: 11,
                          width: 6,
                          child: Image.asset("images/arrow.png")),
                    ),
                  ],
                ),
              ),
              RecentTransactionWidget('images/mcdonalds.png', '\$12.99',
                  'McDonalds', '12:12 AM IST.sep 2'),
              RecentTransactionWidget('images/exchange.png', '\$401.98',
                  'Exchange from INR', '12:12 AM IST.sep 2'),
              RecentTransactionWidget('images/clothing.png', '\$129.99',
                  'H&M clothing', '12:12 AM IST.sep 2'),
            ]),
          ]))
        ]));
  }
}
