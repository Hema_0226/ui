import 'package:flutter/material.dart';
import 'package:visa_appp/screens/05-homescreen.dart';

class LoginPin extends StatefulWidget {
  @override
  _LoginPinState createState() => _LoginPinState();
}

class _LoginPinState extends State<LoginPin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Center(
          child: Container(
            height: 70,
            width: 151,
            child: Image(
              image: AssetImage('images/logo_splash.png'),
            ),
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30.0),
          child: Text(
            'Welcome aboard!',
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: "Poppins-Regular",
              color: Colors.black,
            ),
          ),
        ),
        Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.only(top: 71.0, left: 30),
            child: Text(
              'LOGIN PIN',
              style: TextStyle(
                color: Color(0xff9d9da9),
                fontSize: 10.0,
                fontFamily: 'Poppins-SemiBold',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xffb9b9bf),
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'enter your PIN'),
                keyboardType: TextInputType.phone,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 30.0, left: 20.0, right: 20.0, bottom: 12),
            child: Container(
              height: 42,
              decoration: BoxDecoration(
                color: Color(0xff0066cc),
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Center(
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  },
                  child: Center(
                    child: Text(
                      'Login',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Poppins-SemiBold',
                        fontSize: 12.0,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ])),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Center(
            child: Text(
              'forgot PIN?',
              style: TextStyle(
                fontSize: 13.0,
                fontFamily: 'Poppins-SemiBold',
              ),
            ),
          ),
        ),
      ]),
    ));
  }
}
