import 'dart:ui';

import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:visa_appp/screens/10-currencymanage.dart';
import 'package:visa_appp/widgets/current_balance_widget.dart';
import 'package:visa_appp/widgets/recent_transaction_widget.dart';

import '06-Expense.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Color(0xfff5f5f7),
          body: Stack(children: [
            Container(
              height: MediaQuery.of(context).size.height / 3,
              width: double.infinity,
              child: Image(
                fit: BoxFit.fill,
                image: AssetImage("images/bg1.png"),
              ),
            ),
            SingleChildScrollView(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 64, left: 20, right: 20),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          'current balance',
                          style: TextStyle(
                            color: Color(0xffffffff),
                            fontFamily: "Poppins-Regular",
                            fontSize: 12,
                          ),
                        ),
                      ),
                      Container(
                          height: 11,
                          width: 13,
                          child: Image.asset("images/menu.png")),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 20.0, right: 20.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Color(0xffe3e3e5),
                        ),
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Color(0xfff5f5f7),
                            borderRadius: BorderRadius.circular(15)),
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 15, top: 15, bottom: 15),
                                child: Text(
                                  'YES BANK-2134',
                                  style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: 10,
                                    color: Color(0xff1c1d22),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Container(
                                height: 10,
                                width: 31,
                                child: Image(
                                  image: AssetImage("images/visa.png"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: CurrentBalanceWidget(
                            'images/usa.png', '\$', '599.61', 'EUR'),
                      ),
                      Container(
                          color: Colors.white,
                          child: CurrentBalanceWidget(
                              'images/india.png', '₹', '600', 'IND')),
                      Container(
                          color: Colors.white,
                          child: CurrentBalanceWidget(
                              'images/australia.png', 'A\$', '300', 'AUS')),
                    ]),
                  ),
                ),
                Column(children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 30.0, left: 20, right: 36),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            'recent transactions',
                            style: TextStyle(
                                fontFamily: 'Poppins-Regular',
                                fontSize: 12.0,
                                color: Color(0xff616267)),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExpenseScreen()));
                          },
                          child: Row(
                            children: [
                              Text(
                                'viewall',
                                style: TextStyle(
                                    fontFamily: 'Poppins-Medium',
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff616267)),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                              height: 11,
                              width: 6,
                              child: Image.asset("images/arrow.png")),
                        ),
                      ],
                    ),
                  ),
                  RecentTransactionWidget('images/mcdonalds.png', '\$12.99',
                      'McDonalds', '12:12 AM IST.sep 2'),
                  RecentTransactionWidget('images/exchange.png', '\$401.98',
                      'Exchange from INR', '12:12 AM IST.sep 2'),
                  RecentTransactionWidget('images/clothing.png', '\$129.99',
                      'H&M clothing', '12:12 AM IST.sep 2'),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 30, left: 20, right: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Color(0xffe3e3e5),
                        ),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Text(
                                    'ATM Locator',
                                    style: TextStyle(
                                      fontFamily: "Poppins-SemiBold",
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Color(0xff1c1d22),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 5, left: 20),
                                  child: Container(
                                    height: 28,
                                    width: 115,
                                    child: Text(
                                      'Find a visa™ ATM near our location',
                                      style: TextStyle(
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 10,
                                        color: Color(0xff1c1d22),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, top: 15, bottom: 20),
                                  child: Container(
                                    height: 36,
                                    width: 118,
                                    decoration: BoxDecoration(
                                        color: Color(0xff0066cc),
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Center(
                                      child: Text(
                                        'start looking',
                                        style: TextStyle(
                                          fontFamily: "Poppins-SemiBold",
                                          fontSize: 12,
                                          color: Color(0xffffffff),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 34.7, top: 30),
                              child: Container(
                                  height: 127,
                                  width: 172,
                                  child: Image.asset("images/atm_locator.png")),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30.0, left: 20.0, right: 20.0),
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            width: 1.0,
                            color: Color(0xffe3e3e5),
                          ),
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Column(
                        children: [
                          Row(children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 20.0),
                              child: Text(
                                'Exchange Money',
                                style: TextStyle(
                                  color: Color(0xff1c1d22),
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                          ]),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 5.0,
                                    left: 20.0,
                                    right: 120.0,
                                    bottom: 19.5),
                                child: Container(
                                  width: 200,
                                  child: Text(
                                    'Find Out real-timeExchange rates & Exchange between yours balances',
                                    style: TextStyle(
                                        color: Color(0xff9d9da9),
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 10.0,
                                        letterSpacing: 0),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 1,
                            color: Color(0xffb9b9bf).withOpacity(.2),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 14.5),
                            child: ListTile(
                              title: Wrap(
                                spacing: 12,
                                children: [
                                  Text(
                                    'USD',
                                    style: TextStyle(
                                      fontFamily: "Poppins-Light",
                                      fontSize: 30,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 12.5),
                                    child: Container(
                                        height: 8,
                                        width: 14,
                                        child: Image.asset("images/down.png")),
                                  ),
                                ],
                              ),
                              subtitle: Text(
                                'Balance \$139',
                                style: TextStyle(
                                    fontFamily: "Poppins-Medium",
                                    fontSize: 10.0,
                                    letterSpacing: 0),
                              ),
                              trailing: Text(
                                '1',
                                style: TextStyle(
                                    fontFamily: "Poppins-Light",
                                    fontSize: 30.0,
                                    letterSpacing: 0),
                              ),
                            ),
                          ),
                          Stack(alignment: Alignment.center, children: [
                            Divider(
                              color: Color(0xffb9b9bf).withOpacity(.2),
                              thickness: 1,
                            ),
                            ListTile(
                              title: Chip(
                                label: Text(
                                  '1=0.91',
                                  style: TextStyle(
                                      fontFamily: "Poppins-SemiBold",
                                      fontSize: 10.0),
                                ),
                              ),
                            ),
                          ]),
                          ListTile(
                            title: Wrap(
                              spacing: 12,
                              children: [
                                Text(
                                  'EUR',
                                  style: TextStyle(
                                    fontFamily: "Poppins-Light",
                                    fontSize: 30,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 12.5),
                                  child: Container(
                                      height: 8,
                                      width: 14,
                                      child: Image.asset("images/down.png")),
                                ),
                              ],
                            ),
                            subtitle: Text(
                              'Balance: €20',
                              style: TextStyle(
                                  fontFamily: "Poppins-Medium",
                                  fontSize: 10.0,
                                  letterSpacing: 0),
                            ),
                            trailing: Text(
                              '0.61',
                              style: TextStyle(
                                  fontFamily: "Poppins-Light",
                                  fontSize: 30.0,
                                  letterSpacing: 0),
                            ),
                          ),
                          Divider(
                            thickness: 1,
                            color: Color(0xffb9b9bf).withOpacity(.2),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 14, left: 20, right: 215, bottom: 20),
                            child: Container(
                              height: 36,
                              width: 130,
                              decoration: BoxDecoration(
                                  color: Color(0xff0066cc),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 20),
                                child: Text(
                                  'exchange now',
                                  style: TextStyle(
                                      color: Color(0xffffffff),
                                      fontFamily: "Poppins-SemiBold",
                                      fontSize: 12.0,
                                      letterSpacing: 0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                      padding:
                          const EdgeInsets.only(top: 30, left: 20, right: 36),
                      child: Row(children: [
                        Expanded(
                          child: Text(
                            'offers for you',
                            style: TextStyle(
                                fontFamily: 'Poppins-Regular',
                                fontSize: 12.0,
                                color: Color(0xff616267)),
                          ),
                        ),
                        Text(
                          'viewall',
                          style: TextStyle(
                              fontFamily: 'Poppins-Medium',
                              fontWeight: FontWeight.bold,
                              fontSize: 10.0,
                              color: Color(0xff616267)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                              height: 11,
                              width: 6,
                              child: Image.asset("images/arrow.png")),
                        ),
                      ])),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15, left: 20, right: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xffe3e3e5),
                        ),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Image.asset("images/image.png"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Container(
                      height: 77,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0xffe3e3e5)),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10))),
                      child: ListTile(
                          title: Text(
                            'Apollo pharmacy',
                            style: TextStyle(
                              color: Color(0xff1c1d22),
                              fontFamily: "Poppins-Medium",
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          subtitle: Container(
                            height: 19,
                            child: Wrap(children: [
                              Text(
                                'Up to 15% off* at App',
                                style: TextStyle(
                                  color: Color(0xff616267),
                                  fontFamily: "Poppins-Regular",
                                  fontSize: 13,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 4, right: 11, left: 10),
                                child: Container(
                                    height: 11,
                                    width: 6,
                                    child: Image.asset("images/arrow.png")),
                              ),
                            ]),
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45, left: 22),
                    child: Row(
                      children: [
                        Container(
                            height: 28,
                            width: 105,
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.circular(12)),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 6, left: 29),
                              child: Wrap(children: [
                                Container(
                                    height: 12,
                                    width: 14,
                                    child: Image.asset("images/home.png")),
                                Padding(
                                  padding: const EdgeInsets.only(top: 2),
                                  child: Text(
                                    ' home',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 10.0,
                                        letterSpacing: 0),
                                  ),
                                ),
                              ]),
                            )),
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Container(
                            height: 28,
                            width: 105,
                            decoration: BoxDecoration(
                                color: Color(0xfff5f5f7),
                                borderRadius: BorderRadius.circular(12)),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 6, left: 29),
                              child: Wrap(children: [
                                Container(
                                    height: 12,
                                    width: 14,
                                    child: Image.asset("images/offers.png")),
                                Padding(
                                  padding: const EdgeInsets.only(top: 2),
                                  child: Text(
                                    ' offers',
                                    style: TextStyle(
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 10.0,
                                        letterSpacing: 0),
                                  ),
                                ),
                              ]),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, right: 20),
                          child: Container(
                            height: 28,
                            width: 105,
                            decoration: BoxDecoration(
                                color: Color(0xfff5f5f7),
                                borderRadius: BorderRadius.circular(12)),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 6, left: 29),
                              child: Wrap(children: [
                                Container(
                                    height: 12,
                                    width: 14,
                                    child: Image.asset("images/settings.png")),
                                Padding(
                                  padding: const EdgeInsets.only(top: 2),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ExpenseScreen()));
                                    },
                                    child: Text(
                                      ' settings',
                                      style: TextStyle(
                                          fontFamily: "Poppins-Medium",
                                          fontSize: 10.0,
                                          letterSpacing: 0),
                                    ),
                                  ),
                                )
                              ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ]),
            )
          ])),
    );
  }
}
