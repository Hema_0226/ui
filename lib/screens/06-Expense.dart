import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:visa_appp/screens/07-Atmlocator.dart';
import 'package:visa_appp/widgets/expense_widget.dart';

class ExpenseScreen extends StatefulWidget {
  ExpenseScreen({Key key}) : super(key: key);

  @override
  _ExpenseScreenState createState() => _ExpenseScreenState();
}

class _ExpenseScreenState extends State<ExpenseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff5f5f7),
        body: Stack(children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: double.infinity,
            child: Image(
              fit: BoxFit.fill,
              image: AssetImage("images/bg.png"),
            ),
          ),
          SingleChildScrollView(
              child: Column(children: [
            Padding(
              padding: const EdgeInsets.only(top: 59, left: 20, right: 20),
              child: Row(
                children: [
                  Container(
                      height: 16,
                      width: 9,
                      child: Image.asset("images/back.png")),
                  Expanded(
                    child: Center(
                      child: Text(
                        'transaction details',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontFamily: "Poppins-Regular",
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                  Container(
                      height: 11,
                      width: 13,
                      child: Image.asset("images/more.png")),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 21.0, left: 20.0, right: 20.0, bottom: 20.0),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        width: 1.0,
                        color: Color(0xffe3e3e5),
                      ),
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 15,
                    ),
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 93),
                        child: Row(children: [
                          Image.asset("images/success.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 1),
                            child: Text(
                              'transaction successful',
                              style: TextStyle(
                                color: Color(0xff1c1d22),
                                fontFamily: "Poppins-Medium",
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14.5),
                        child: Divider(
                          thickness: 1,
                          color: Color(0xffb9b9bf),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.5),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AtmLocator()));
                            },
                            child: Image.asset("images/exchange.png")),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 24.5),
                        child: Text(
                          'Exchange from INR',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontFamily: "Poppins-Regular",
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Color(0xffb9b9bf),
                      ),
                      ListTile(
                        title: Text(
                          'Amount',
                          style: TextStyle(
                            color: Color(0xff9d9da9),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                        trailing: Text(
                          '\$12.99',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          'Date',
                          style: TextStyle(
                            color: Color(0xff9d9da9),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                        trailing: Text(
                          'Sep 2, 2019',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          'Time',
                          style: TextStyle(
                            color: Color(0xff9d9da9),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                        trailing: Text(
                          '06:49 PM',
                          style: TextStyle(
                            color: Color(0xff1c1d22),
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ]),
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Color(0xffe3e3e5),
                    ),
                    borderRadius: BorderRadius.circular(15)),
                child: Column(children: [
                  Container(
                    height: 35,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xff1464b0),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10))),
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(
                              'PREPAID TRAVEL CARD',
                              style: TextStyle(
                                color: Color(0xffffffff),
                                fontFamily: "Poppins-Medium",
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: Text(
                            '\$USD',
                            style: TextStyle(
                              color: Color(0xffffffff),
                              fontFamily: "Poppins-Medium",
                              fontSize: 10,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 40, left: 15, bottom: 40, right: 15),
                    child: Row(
                      children: [
                        Text(
                          '4829 XXXX XXXX 0124',
                          style: TextStyle(
                            color: Color(0xff616267),
                            fontFamily: "Poppins-Light",
                            fontSize: 16,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 104),
                          child: Image.asset("images/visa.png"),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Container(
                    height: 49,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xffe3e3e5)),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Text(
                            'other transaction details',
                            style: TextStyle(
                              fontFamily: "Poppins-Medium",
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 13, right: 15),
                          child: Container(
                              height: 14,
                              width: 8,
                              child: Image.asset("images/arrow.png")),
                        ),
                      ],
                    ),
                  ),
                ),
                ExpenseWidget('add receipt'),
                ExpenseWidget('write a note'),
              ],
            ),
          ]))
        ]));
  }
}
