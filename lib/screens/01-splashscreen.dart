import 'dart:async';

import 'package:flutter/material.dart';

import '02-Loginscreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 6),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen())));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 371),
            child: Center(
              child: Container(
                height: 70.0,
                width: 151,
                child: Image(
                  image: AssetImage('images/logo_splash.png'),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 67.0, horizontal: 40.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Image(
                    image: AssetImage('images/verified_by_visa.png'),
                    width: 79.5,
                    height: 30.0,
                  ),
                ),
                Container(
                  child: Image(
                    image: AssetImage('images/globalsign_secure.png'),
                    width: 75.0,
                    height: 30.0,
                  ),
                ),
                Container(
                  child: Image(
                    image: AssetImage('images/panacea_pci.png'),
                    width: 56.3,
                    height: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
