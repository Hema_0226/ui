import 'dart:ui';

import 'package:flutter/material.dart';

class CardPreferences extends StatefulWidget {
  CardPreferences({Key key}) : super(key: key);

  @override
  _CardPreferencesState createState() => _CardPreferencesState();
}

class _CardPreferencesState extends State<CardPreferences> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff5f5f7),
        body: SingleChildScrollView(
          child: Stack(children: [
            Container(
              child: Image(
                image: AssetImage("images/bg1.png"),
              ),
            ),
            Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 64, left: 20, right: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'card settings',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontFamily: "Poppins-Regular",
                          fontSize: 12,
                        ),
                      ),
                    ),
                    Container(
                        height: 11,
                        width: 13,
                        child: Image.asset("images/menu.png")),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
                child: Column(children: [
                  Container(
                    height: 35,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xff1464b0),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10))),
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(
                              'PREPAID TRAVEL CARD',
                              style: TextStyle(
                                color: Color(0xffffffff),
                                fontFamily: "Poppins-Medium",
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: Text(
                            '\$USD',
                            style: TextStyle(
                              color: Color(0xffffffff),
                              fontFamily: "Poppins-Medium",
                              fontSize: 10,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15))),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 70, top: 40),
                        child: Row(
                          children: [
                            Text(
                              '4829 XXXX XXXX 0124',
                              style: TextStyle(
                                color: Color(0xff616267),
                                fontFamily: "Poppins-Light",
                                letterSpacing: 2,
                                fontSize: 16,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Container(
                                  width: 15,
                                  height: 20,
                                  child: Image.asset("images/lock_2.png")),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 40, left: 20, bottom: 20),
                        child: Row(children: [
                          Text(
                            '\$',
                            style: TextStyle(
                              color: Color(0xff1a1f71),
                              fontFamily: "Poppins-Regular",
                              fontSize: 12,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 3, left: 5),
                            child: Text(
                              '994.91',
                              style: TextStyle(
                                color: Color(0xff000000),
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 201, right: 20, top: 3),
                            child: Image.asset("images/visa.png"),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 20),
                    child: Text(
                      'lock card',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ]),
          ]),
        ));
  }
}
